package ir.aflaksoft.aflak2.ui.EntryEstate;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class EntryEstateViewModel extends ViewModel {
    private MutableLiveData<String> mText;

    public EntryEstateViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is gallery fragment");
    }

    public LiveData<String> getString(){
        return mText;
    }
}
