package ir.aflaksoft.aflak2.ui.EntryEstate;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ir.aflaksoft.aflak2.R;

public class EntryEstate extends Fragment {

    private EntryEstateViewModel mViewModel;

    public static EntryEstate newInstance() {
        return new EntryEstate();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.entry_estate_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(EntryEstateViewModel.class);
        // TODO: Use the ViewModel
    }

}
